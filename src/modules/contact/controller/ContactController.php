<?php

namespace modules\contact\controller;

use common\Controller;

class ContactController extends Controller {

    protected $moduleName = 'page';

    function defaultAction() {

        $model = $this->getModel();
        $model->title ='Contact us';

        $variables['obj'] = $model;
        $this->Template->render('contact' . DS . 'view' . DS . 'contact',
                                $variables);
    }
}
