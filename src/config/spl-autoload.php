<?php

spl_autoload_register(function($classNameWithNamespace) {

    $fileFullPath = ROOT_PATH
                  . str_replace('\\', '/', $classNameWithNamespace) . '.php';

    if(file_exists($fileFullPath)) {
        include $fileFullPath;
    }
});
