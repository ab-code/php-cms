<?php

namespace common;
use common\DatabaseConnection;

class Controller {

    protected $moduleName;
    protected $entity_id;
    public $Template;

    public function runAction($action) {
        $methodName = $action . 'Action';

        if(method_exists($this, $methodName)) {
            $this->$methodName();
        }
        else {
            echo "Action not found";
        }
    }

    protected function getModel() {
        $dbh = DatabaseConnection::getInstance();
        $db = $dbh->getConnection();

        $modelClassUsePath = 'modules\\' . $this->moduleName .
                           '\\model\\' . ucfirst($this->moduleName);

        $model = new $modelClassUsePath($db);
        return $model;
    }

    public function setEntityId($id) {
        $this->entity_id = $id;
    }
}
