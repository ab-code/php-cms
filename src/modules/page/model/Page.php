<?php

namespace modules\page\model;

use common\Entity;

class Page extends Entity {

    public $tableName = 'pages';
    public $fields = [
        'id',
        'title',
        'content',
    ];
}
