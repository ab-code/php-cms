<?php

namespace common;

final class DatabaseConnection {

    static private $dbc;
    static private $instance = null;

    public function __construct() {}
    public function __clone() {}

    public static function getInstance() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function getConnection() {
        return self::$dbc;
    }

    public static function connect($host, $dbname, $username, $password) {
        $dbh = new \PDO(
            "mysql:host=$host;dbname=$dbname", $username, $password
        );
        self::$dbc = $dbh;
    }
}
