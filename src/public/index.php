<?php

use common\DatabaseConnection;
use common\Router;
use common\Template;

define('DS', DIRECTORY_SEPARATOR);
include(__DIR__ . DS . '..' . DS . 'config' . DS . 'init.php');

$dbh = DatabaseConnection::getInstance();
$db = $dbh->getConnection();

$router = new Router($db);
$seo_name = $_GET['seo_name'] ?? 'home';

$router->find('pretty_url', $seo_name);

$action = $router->action != '' ? $router->action : 'default';

$classFilename = ucfirst($router->module) . 'Controller';
$classUsePath = 'modules\\' . $router->module . '\\controller\\' . $classFilename;

if(class_exists($classUsePath)) {
    $controller = new $classUsePath();
    $controller->setEntityId($router->entity_id);
    $controller->Template = new Template('layout'. DS . 'default');
    $controller->runAction($action);
} else {
    echo "Controller does not exists !";
    die();
}

