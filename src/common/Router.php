<?php

namespace common;

use common\Entity;

class Router extends Entity {
    public $fields = [
        'id',
        'module',
        'action',
        'entity_id',
        'pretty_url',
    ];

    public $tableName = 'router';
}
