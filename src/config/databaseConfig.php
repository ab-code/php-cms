<?php

use common\DatabaseConnection;

DatabaseConnection::connect(
    getenv('MYSQL_HOST', true) ? getenv('MYSQL_HOST') : 'mysql-db',
    'cms_php',
    'root',
    getenv('MYSQL_ROOT_PASSWORD', true) ? getenv('MYSQL_ROOT_PASSWORD') : ''
);
