<?php

namespace modules\page\controller;

use common\Controller;
use common\Template;

class PageController extends Controller {

    protected $moduleName = 'page';

    public function defaultAction() {

        $model = $this->getModel();

        $data = $model->find('id', $this->entity_id);
        $model->setObjectProperties($data);

        $variables['obj'] = $model;

        $this->Template->render('page'. DS .'view'. DS .'static-page',
                               $variables);
    }
}
