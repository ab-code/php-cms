<?php

namespace common;

class Template {

    public function __construct(
        public $defaultTemplate = 'layout' . DS . 'default',
    ) {}

    public function render($template, $variables) {
        extract($variables);

        $mainTemplate = VIEW_PATH . $this->defaultTemplate . '.html';

        if(file_exists($mainTemplate)) {
            include $mainTemplate;
        }
    }
}
