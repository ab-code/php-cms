<?php

namespace common;

class Entity {
    public $fields;
    public $tableName;

    public function __construct(
        private $db,
    ) {}

    public function setObjectProperties($fetchedArray) {
        if($fetchedArray) {
            foreach($this->fields as $field) {
                $this->$field = $fetchedArray[$field];
            }
        }
    }

    function find($fieldName = 'id', $fieldValue = null) {

        $sql = 'SELECT * FROM ' . $this->tableName;

        if($fieldValue != null) {
            $sql .= ' WHERE ' . $fieldName . ' = :fieldValue';
            $statement = $this->db->prepare($sql);
            $statement->bindParam('fieldValue', $fieldValue);
        } else {
            $statement = $this->db->prepare($sql);
        }

        $statement->execute();
        $data = $statement->fetch(\PDO::FETCH_ASSOC);

        $this->setObjectProperties($data);
    }
}

